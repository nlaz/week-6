'''cmake
cmake_minimum_required(VERSION 3.15)
project(SearchApp)

set(CMAKE_CXX_STANDARD 11)

add_executable(SearchApp a.cpp)

target_include_directories(SearchApp PRIVATE include) 
